import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;


class AdvancedSorting {
	
	public static void sort(Sort sortType,String args[]) throws Exception{
		Sort in=new Sort();
		in=sortType;
		in.validate(args);
		in.readFile();
		in.sortArray();
	}
	
	public static void main(String [] args){
		try {
			switch (args[1]){
				case "qsort":
					sort(new QuickSort(), args);
					break;
				case "bubble":
					sort(new BubbleSort(), args);
					break;
				case "shift":
					sort(new ShiftSort(), args);
					break;
				default:
					System.err.println("There is no such method");
					System.exit(1);
			}
		} catch (Exception e) {
			System.err.println(e); 
			System.exit(1);
		}
	}
}

class Sort{
	private String fileName="";
	private ArrayList<Integer> array=new ArrayList<Integer>();
	
	public void readFile() throws FileNotFoundException{
		try{
	        Scanner in=new Scanner(new File("D:\\Workspace\\Advanced Sorting\\src\\"+fileName));
	        while (in.hasNextLine()){
	        	array.add(Integer.parseInt(in.nextLine()));
	        }
		}
        catch(Exception e){
        	throw new FileNotFoundException("No file with name: "+fileName+" is found");
		}
	}
	
	public void validate(String [] args) throws Exception{
		try{
			if(args.length>2 || args.length<2)
				throw new Exception("Length of args[] must be 2");
			fileName=args[0];
		}	
		catch(Exception e){
			System.err.println(e); 
			System.exit(1);
		}
	}
	
	public void sortArray() throws Exception{
		
	}
	
	public ArrayList<Integer> getArray(){ 
		return array;
	}
}

class QuickSort extends Sort{
	private ArrayList<Integer> array;

	public void sortArray() throws Exception {
		this.array=super.getArray();
	    if (array==null||array.size()==0)
	    	throw new Exception("Set the value in the file");
	    int size=this.array.size();
	    System.out.println("Before Quick sorting: "+super.getArray());
	    System.out.println("After Quick sorting: "+qSort(0, size-1));
	}
	
	public ArrayList<Integer> qSort(int start, int end){
		try{
			if (start>=end)
	            return array;
	        int i=start,j=end;
	        int cur=i-(i-j)/2;
		    while (i<j){
	            while(i<cur&&(array.get(i)<=array.get(cur))){
	            	i++;
	            }
	            while(j>cur&&(array.get(cur)<=array.get(j))) {
	                j--;
	            }
	            if (i<j) {
	                int temp=array.get(i);
	                array.set(i, array.get(j));
	                array.set(j, temp);
	                if (i==cur)
	                   cur=j;
	                else if (j==cur)
	                   cur=i;
	            }
	        }
	        qSort(start, cur);
	        qSort(cur+1, end);
		}
		catch(Exception e){
			System.err.println(e); 
			System.exit(1);
		}
		return array;
	}
}

class BubbleSort extends Sort{
	private ArrayList<Integer> array;

	public void sortArray() throws Exception {
		this.array=super.getArray();
	    if (array==null||array.size()==0)
	    	throw new Exception("Set the value in the file");
	    System.out.println("Before Bubble sorting: "+super.getArray());
		for(int i=0;i<this.array.size()-1;i++)
			for(int j=i+1;j<this.array.size();j++)
				if(this.array.get(i)>this.array.get(j)){
					int temp=this.array.get(i);
					this.array.set(i,this.array.get(j));
					this.array.set(j,temp);
				}			
	    System.out.println("After Bubble sorting: "+array);
	}	
}

class ShiftSort extends Sort{
	private ArrayList<Integer> array;

	public void sortArray() throws Exception {
		this.array=super.getArray();
	    if (array==null||array.size()==0)
	    	throw new Exception("Set the value in the file");
		System.out.println("Before Shift sorting: "+super.getArray());
	    for(int i=1;i<this.array.size();i++){
	        for(int j=i; j>0 && this.array.get(j-1)>this.array.get(j);j--){
	            int temp=this.array.get(j-1);
	            this.array.set(j-1,this.array.get(j));
	            this.array.set(j,temp);
	        }
	    }
	    System.out.println("After Shift sorting: "+this.array);
	}	
}







